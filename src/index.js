import React from 'react';
import Votacao from './pages/Votacao';

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Unrecognized WebSocket']);

const App = () => <Votacao />;

export default App;
