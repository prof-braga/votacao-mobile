import React, { Component } from "react";
import {
  Text,
  Image,
  ImageBackground,
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";

import { api, ioURL } from "../../services/api";

import socket from "socket.io-client";
import styles from "./styles";
import logo from "../../assets/logo.png";

import Icon from "react-native-vector-icons/MaterialIcons";

class Votacao extends Component {
  state = {
    registros: []
  };

  async componentDidMount() {
    this.iniciarIO();

    const res = await api.get("/musica");
    let registros = this.ordenar(res.data);

    this.setState({
      registros,
    });
  }

  iniciarIO = () => {
    const io = socket(ioURL);

    io.on("scoreMusica", data => {
      this.updateState(data._id, data)
    });
  };

  diminuir = async i => {
    // Decrementa um ponto
    i.score = i.score - 1;

    const res = await api.put("/musica/score/" + i._id, i);
    this.updateState(i._id, res.data)
  };

  aumentar = async i => {
    // Incrementa um ponto
    i.score = i.score + 1;

    const res = await api.put("/musica/score/" + i._id, i);
    this.updateState(i._id, res.data)
  };

  updateState = (id, data) => {
    let registros = [...this.state.registros.filter(item => { return item._id !== id; }), data];
    registros = this.ordenar(registros);

    this.setState({
      registros,
    });
  }

  ordenar = colDados => {
    return colDados.sort(function (item1, item2) {
      return item2.score - item1.score;
    });
  };

  renderItem = (item, i) => {
    return (
      <View style={styles.linha}>
        <Text style={i == 0 ? styles.itemTextScoreFirst : styles.itemTextScore}>
          {item.score}
        </Text>

        <View style={{ flexDirection: "column", flex: 1, padding: 5 }}>
          <Text style={styles.itemTextTitulo}>{item.titulo}</Text>
          <Text style={styles.itemText}>Autor: {item._autor.nome}</Text>
          <Text style={styles.itemText}>Album: {item.album}</Text>
        </View>

        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <TouchableOpacity style={styles.arrow} onPress={() => this.diminuir(item)}          >
            <Icon name="arrow-downward" size={24} color="darkred" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.arrow} onPress={() => this.aumentar(item)}          >
            <Icon name="arrow-upward" size={24} color="darkgreen" />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require("../../assets/fundo.jpg")} style={styles.backgroundImage}>
          <View style={{ marginBottom: 125 }}>
            <View style={{ marginTop: 10, alignSelf: "center", flexDirection: "row", justifyContent: "space-between" }}>
              <Image style={styles.logo} source={logo} />
              <Text style={styles.titulo}>Musicas</Text>
            </View>

            <View style={{ marginTop: 10 }}>
              <FlatList
                data={this.state.registros}
                keyExtractor={t => t._id}
                renderItem={({ item, index }) => this.renderItem(item, index)}
              />
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

export default Votacao;
