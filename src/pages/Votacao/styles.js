import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "cover"
  },
  titulo: {
    borderLeftColor: "#CCC",
    borderLeftWidth: 1,
    fontSize: 20,
    fontWeight: "bold",
    paddingLeft: 10,
    paddingTop: 10
  },
  logo: {
    width: 100,
    height: 50,
    marginRight: 10
  },
  linha: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#FFF",
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    borderBottomWidth: 1
  },
  arrow: {
    marginTop: 17,
    marginBottom: 17,
    borderColor: "#D5D5D5",
    borderWidth: 1,
    padding: 5,
    borderRadius: 50
  },
  itemTextTitulo: {
    fontWeight: "bold",
    fontSize: 18
  },
  itemText: {
    justifyContent: "flex-start"
  },
  itemTextScoreFirst: {
    marginTop: 17,
    marginBottom: 17,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "green",
    fontWeight: "bold",
    color: "white"
  },
  itemTextScore: {
    marginTop: 17,
    marginBottom: 17,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "orange",
    fontWeight: "bold",
    color: "white"
  }
});

export default styles;
