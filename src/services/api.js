import axios from 'axios';

// genymotion
// export const ioURL = 'http://10.10.20.154:4000'

// ngrok
// export const ioURL = 'http://c691a5aa.ngrok.io'

export const ioURL = 'https://votacao-backend.herokuapp.com';

export const api = axios.create({
  baseURL: ioURL + '/api'
});
